# [Kotlin] Flashcards
A simple Android app to help learning HSK 1 Chinese vocabulary. Writen in Kotlin using MVVM and Android architecture components.

![Flashcards app screen](img/screen1.jpg)
![Flashcards app screen](img/screen2.jpg)
![Flashcards app screen](img/screen3.jpg)

## Disclaimer

The software is provided "as is", and without warranty of any kind. In
no event shall the author be liable for any claim, tort, damage, or any
other liability.

By using the program, you agree to the above terms and conditions.
