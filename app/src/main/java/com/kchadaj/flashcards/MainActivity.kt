package com.kchadaj.flashcards

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.kchadaj.flashcards.databinding.ActivityMainBinding
import com.kchadaj.flashcards.items.AnswerItem
import com.kchadaj.flashcards.utils.recycler.MultiTypeAdapter

class MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModelContract by viewModels<MainViewModel> { MainViewModelFactory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main).let { binding ->
            binding.lifecycleOwner = this
            binding.viewModel = viewModel
            configureAnswersRecycler(binding, this)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        menu.findItem(R.id.pinyinSwitch)?.isChecked = viewModel.secondaryVisibility.value ?: false
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.reset -> viewModel.reset()
            R.id.pinyinSwitch -> {
                item.isChecked = item.isChecked.not()
                viewModel.showPinyin(item.isChecked)
            }
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    private fun configureAnswersRecycler(binding: ActivityMainBinding, lifecycleOwner: LifecycleOwner) {
        val context = binding.root.context
        val columnsCount = context.resources.getInteger(R.integer.answers_columns_count)
        binding.optionsRecycler.layoutManager = GridLayoutManager(context, columnsCount)
        val adapter = MultiTypeAdapter<AnswerItem>().apply { onItemClick = viewModel::onItemClick }
        binding.optionsRecycler.adapter = adapter
        viewModel.answerItems.observe(lifecycleOwner, Observer(adapter::submitList))
    }
}
