package com.kchadaj.flashcards

import androidx.lifecycle.LiveData
import com.kchadaj.flashcards.items.AnswerItem
import com.kchadaj.flashcards.utils.label.Label
import com.kchadaj.flashcards.utils.recycler.ItemClickListener

interface MainViewModelContract : ItemClickListener<AnswerItem> {
    val primaryText: LiveData<Label>
    val secondaryText: LiveData<Label>
    val secondaryVisibility: LiveData<Boolean>
    val answerItems: LiveData<List<AnswerItem>>
    val pointsLabel: LiveData<Label>

    fun reset()
    fun showPinyin(show: Boolean)
}
