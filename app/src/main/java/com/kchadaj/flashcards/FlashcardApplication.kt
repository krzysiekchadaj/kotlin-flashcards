package com.kchadaj.flashcards

import android.app.Application
import com.kchadaj.flashcards.data.DictionaryRepository
import com.kchadaj.flashcards.data.LocalDictionaryDataSource
import com.kchadaj.flashcards.data.Repository

class FlashcardApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        REPOSITORY = DictionaryRepository(LocalDictionaryDataSource(this))
    }

    companion object {
        internal lateinit var REPOSITORY: Repository
    }
}
