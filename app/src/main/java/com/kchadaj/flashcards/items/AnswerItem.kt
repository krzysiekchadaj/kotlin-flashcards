package com.kchadaj.flashcards.items

import com.kchadaj.flashcards.R
import com.kchadaj.flashcards.utils.recycler.Item

data class AnswerItem(
    val answer: String,
    val isVisible: Boolean = true
) : Item {

    override val layoutId: Int = R.layout.item_answer
}
