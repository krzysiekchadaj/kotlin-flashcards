package com.kchadaj.flashcards

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kchadaj.flashcards.data.Repository
import com.kchadaj.flashcards.entities.Flashcard
import com.kchadaj.flashcards.entities.Word
import com.kchadaj.flashcards.items.AnswerItem
import com.kchadaj.flashcards.points.PointsCalculator
import com.kchadaj.flashcards.utils.extensions.mutableLiveDataOf
import com.kchadaj.flashcards.utils.label.Label

class MainViewModel(private val repository: Repository) : MainViewModelContract, ViewModel() {

    override val primaryText = MutableLiveData<Label>()
    override val secondaryText = MutableLiveData<Label>()
    override val secondaryVisibility = mutableLiveDataOf(true)
    override val pointsLabel = MutableLiveData<Label>()
    override val answerItems = MutableLiveData<List<AnswerItem>>()
    private var answers: List<AnswerItem> = emptyList()
        set(value) {
            answerItems.postValue(value)
            field = value
        }
    private val pointsCalculator = PointsCalculator(Flashcard.POINTS)
    private var currentFlashcard = repository.getRandomFlashcard().also {
        updatePoints(0)
        showFlashcard(it)
    }

    override fun onItemClick(item: AnswerItem) {
        if (currentFlashcard.isAnswerCorrect(item)) {
            pointsCalculator.calculatePoints().let(::updatePoints)
            pointsCalculator.onAnswerCorrect()
            getAndShowNewFlashcard()
        } else {
            pointsCalculator.onAnswerWrong()
            hideOption(item)
        }
    }

    override fun reset() {
        pointsCalculator.reset()
        updatePoints(0)
        showAllOptions()
    }

    override fun showPinyin(show: Boolean) = secondaryVisibility.postValue(show)

    private fun Flashcard.isAnswerCorrect(answer: AnswerItem) = answer.answer == solution.chinese

    private fun updatePoints(points: Int) {
        Label.of(R.string.points_label, points).let(pointsLabel::postValue)
    }

    private fun getAndShowNewFlashcard() {
        currentFlashcard = repository.getRandomFlashcard()
        showFlashcard(currentFlashcard)
    }

    private fun showFlashcard(flashcard: Flashcard) {
        Label.of(flashcard.solution.english).let(primaryText::postValue)
        Label.of(flashcard.solution.pinyin).let(secondaryText::postValue)
        answers = flashcard.answers.map(Word::chinese).map { AnswerItem(it) }
        showAllOptions()
    }

    private fun showAllOptions() {
        answers = answers.map { it.copy(isVisible = true) }
    }

    private fun hideOption(item: AnswerItem) {
        answers = answers.map {
            if (it == item) it.copy(isVisible = false) else it
        }
    }
}
