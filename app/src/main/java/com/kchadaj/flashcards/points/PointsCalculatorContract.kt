package com.kchadaj.flashcards.points

interface PointsCalculatorContract {
    fun calculatePoints(): Int
    fun onAnswerCorrect()
    fun onAnswerWrong()
    fun reset()
}
