package com.kchadaj.flashcards.points

class PointsCalculator(private val maximumPointsPerQuestion: Int) : PointsCalculatorContract {

    private var totalPoints = 0
    private var answerAttempts = 0

    override fun calculatePoints(): Int {
        val currentQuestionPoints = maximumPointsPerQuestion - answerAttempts
        totalPoints += currentQuestionPoints
        if (currentQuestionPoints == 0) {
            totalPoints = 0
        }
        return totalPoints
    }

    override fun onAnswerCorrect() {
        answerAttempts = 0
    }

    override fun onAnswerWrong() {
        answerAttempts++
    }

    override fun reset() {
        totalPoints = 0
        answerAttempts = 0
    }
}
