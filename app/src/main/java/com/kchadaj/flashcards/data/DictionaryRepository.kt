package com.kchadaj.flashcards.data

import com.kchadaj.flashcards.entities.Flashcard
import com.kchadaj.flashcards.entities.Word

class DictionaryRepository(dataSource: DataSource) : Repository {

    private var words = dataSource.dictionary.shuffled()
    private var index = 0

    override fun getRandomFlashcard(): Flashcard {
        if (words.size - index < Flashcard.ANSWERS_AMOUNT) {
            words = words.shuffled()
            index = 0
        }
        val solution = getRandomWord()
        return Flashcard(solution, buildAnswers(solution))
    }

    private fun getRandomWord(): Word {
        val w = words[index]
        index++
        return w
    }

    private fun buildAnswers(solution: Word): List<Word> {
        val wrongAnswers = Array(Flashcard.ANSWERS_AMOUNT - 1) { getRandomWord() }
        val answers = wrongAnswers.asList() + solution
        return answers.shuffled()
    }
}
