package com.kchadaj.flashcards.data

import com.kchadaj.flashcards.entities.Flashcard

interface Repository {
    fun getRandomFlashcard(): Flashcard
}
