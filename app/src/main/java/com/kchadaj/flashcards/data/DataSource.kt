package com.kchadaj.flashcards.data

import com.kchadaj.flashcards.entities.Word

interface DataSource {
    val dictionary: List<Word>
}
