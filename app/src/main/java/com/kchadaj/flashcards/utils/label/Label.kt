package com.kchadaj.flashcards.utils.label

import android.content.Context
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.databinding.BindingAdapter

interface Label {

    fun build(context: Context): String

    companion object {
        fun of(text: String): Label = TextLabel(text)
        fun of(@StringRes textId: Int, vararg formatArgs: Any): Label = ResourceLabel(textId, formatArgs.toList())
    }
}

@BindingAdapter("android:text")
fun setLabel(view: TextView, label: Label?) {
    view.text = label?.build(view.context)
}
