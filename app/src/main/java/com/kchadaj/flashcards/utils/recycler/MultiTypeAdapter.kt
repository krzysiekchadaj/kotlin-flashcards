package com.kchadaj.flashcards.utils.recycler

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.ListAdapter

class MultiTypeAdapter<T : Item> : ListAdapter<T, MultiTypeViewHolder<T>>(DiffCallback<T>()) {

    var onItemClick: (T) -> Unit = { Unit }

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MultiTypeViewHolder<T> {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ViewDataBinding>(layoutInflater, viewType, parent, false)
        return MultiTypeViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MultiTypeViewHolder<T>, position: Int) {
        val item = getItem(position)
        holder.bind(item)
        holder.itemView.setOnClickListener { onItemClick(item) }

    }

    override fun getItemViewType(position: Int): Int = getItem(position).layoutId
}
