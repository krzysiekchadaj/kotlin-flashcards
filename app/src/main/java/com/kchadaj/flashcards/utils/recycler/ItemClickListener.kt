package com.kchadaj.flashcards.utils.recycler

interface ItemClickListener<T: Item> {
    fun onItemClick(item: T)
}
