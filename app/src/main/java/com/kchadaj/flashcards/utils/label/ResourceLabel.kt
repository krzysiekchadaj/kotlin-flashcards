package com.kchadaj.flashcards.utils.label

import android.content.Context

internal data class ResourceLabel(private val textId: Int, private val formatArgs: List<Any>) : Label {

    override fun build(context: Context) = context.getString(textId, *formatArgs.toTypedArray())
}
