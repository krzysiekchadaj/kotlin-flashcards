package com.kchadaj.flashcards.utils.recycler

interface Item {
    val id: Int get() = hashCode()
    val layoutId: Int
}
