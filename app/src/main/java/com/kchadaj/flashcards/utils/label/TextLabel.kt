package com.kchadaj.flashcards.utils.label

import android.content.Context

internal data class TextLabel(private val text: String) : Label {

    override fun build(context: Context) = text
}
