package com.kchadaj.flashcards.utils.recycler

import androidx.recyclerview.widget.DiffUtil

class DiffCallback<T : Item> : DiffUtil.ItemCallback<T>() {
    override fun areItemsTheSame(oldItem: T, newItem: T) = oldItem.id == newItem.id
    override fun areContentsTheSame(oldItem: T, newItem: T) = oldItem.id == newItem.id
}
