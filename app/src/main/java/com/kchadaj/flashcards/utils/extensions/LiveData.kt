package com.kchadaj.flashcards.utils.extensions

import androidx.lifecycle.MutableLiveData

fun <T> mutableLiveDataOf(initialValue: T) = MutableLiveData<T>().also { it.value = initialValue }
