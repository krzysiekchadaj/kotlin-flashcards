package com.kchadaj.flashcards

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

@Suppress("UNCHECKED_CAST")
object MainViewModelFactory : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T = when {
        modelClass.isAssignableFrom(MainViewModel::class.java) -> MainViewModel(
            FlashcardApplication.REPOSITORY
        ) as T
        else -> throw IllegalArgumentException("Unknown view model class $modelClass")
    }
}
