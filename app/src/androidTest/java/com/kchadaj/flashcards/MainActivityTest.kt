package com.kchadaj.flashcards

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.kchadaj.flashcards.data.Repository
import com.kchadaj.flashcards.entities.Flashcard
import com.kchadaj.flashcards.entities.Word
import com.kchadaj.flashcards.utils.ReplaceRepositoryRule
import com.kchadaj.flashcards.utils.onRecyclerViewItem
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.not
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    private val repository = mock<Repository> {
        on { getRandomFlashcard() } doReturn Flashcard(
            solution = Word("s_c", "s_p", "s_e"),
            answers = listOf(
                Word("a1_c", "a1_p", "a1_e"),
                Word("a2_c", "a2_p", "a2_e"),
                Word("a3_c", "a3_p", "a3_e"),
                Word("s_c", "s_p", "s_e")
            )
        )
    }
    @get:Rule
    val ruleChain: RuleChain = RuleChain
            .outerRule(ReplaceRepositoryRule(repository))
            .around(ActivityScenarioRule<MainActivity>(MainActivity::class.java))

    @Test
    fun flashcardIsDisplayed() {
        onView(withId(R.id.questionWordPrimary)).check(matches((withText("s_e"))))
        onView(withId(R.id.questionWordSecondary)).check(matches((withText("s_p"))))
        onRecyclerViewItem(R.id.optionsRecycler, 0, R.id.itemAnswer).check(matches(allOf(withText("a1_c"), isDisplayed())))
        onRecyclerViewItem(R.id.optionsRecycler, 1, R.id.itemAnswer).check(matches(allOf(withText("a2_c"), isDisplayed())))
        onRecyclerViewItem(R.id.optionsRecycler, 2, R.id.itemAnswer).check(matches(allOf(withText("a3_c"), isDisplayed())))
        onRecyclerViewItem(R.id.optionsRecycler, 3, R.id.itemAnswer).check(matches(allOf(withText("s_c"), isDisplayed())))
        onView(withId(R.id.pointsLabel)).check(matches(withText("0 pts")))
    }

    @Test
    fun onWrongAnswerClick_itDisappears() {
        onRecyclerViewItem(R.id.optionsRecycler, 0, R.id.itemAnswer).perform(click())
        onRecyclerViewItem(R.id.optionsRecycler, 0, R.id.itemAnswer).check(matches(allOf(withText("a1_c"), not(isDisplayed()))))
    }
}
