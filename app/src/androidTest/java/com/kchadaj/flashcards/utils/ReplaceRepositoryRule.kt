package com.kchadaj.flashcards.utils

import com.kchadaj.flashcards.FlashcardApplication
import com.kchadaj.flashcards.data.Repository
import org.junit.rules.ExternalResource

open class ReplaceRepositoryRule(private val repository: Repository) : ExternalResource() {

    override fun before() {
        FlashcardApplication.REPOSITORY = repository
    }
}
