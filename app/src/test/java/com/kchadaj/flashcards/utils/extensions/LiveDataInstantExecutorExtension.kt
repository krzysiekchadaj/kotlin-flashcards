package com.kchadaj.flashcards.utils.extensions

import androidx.arch.core.executor.ArchTaskExecutor
import androidx.arch.core.executor.TaskExecutor
import io.kotlintest.Spec
import io.kotlintest.extensions.TestListener
import org.junit.jupiter.api.extension.AfterAllCallback
import org.junit.jupiter.api.extension.BeforeAllCallback
import org.junit.jupiter.api.extension.ExtensionContext

class LiveDataInstantExecutorExtension : BeforeAllCallback, AfterAllCallback, TestListener {

    override fun beforeAll(context: ExtensionContext) {
        ArchTaskExecutor.getInstance().setDelegate(InstantExecutor())
    }

    override fun afterAll(context: ExtensionContext) {
        ArchTaskExecutor.getInstance().setDelegate(null)
    }

    override fun beforeSpec(spec: Spec) {
        ArchTaskExecutor.getInstance().setDelegate(InstantExecutor())
    }

    override fun afterSpec(spec: Spec) {
        ArchTaskExecutor.getInstance().setDelegate(null)
    }

    private class InstantExecutor : TaskExecutor() {
        override fun executeOnDiskIO(runnable: Runnable) = runnable.run()

        override fun postToMainThread(runnable: Runnable) = runnable.run()

        override fun isMainThread() = true
    }
}
