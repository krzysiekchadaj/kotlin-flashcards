package com.kchadaj.flashcards.points

import com.kchadaj.flashcards.utils.extensions.LiveDataInstantExecutorExtension
import io.kotlintest.IsolationMode
import io.kotlintest.specs.DescribeSpec
import org.junit.jupiter.api.Assertions.assertEquals

internal class PointsCalculatorTest : DescribeSpec() {

    override fun listeners() = listOf(LiveDataInstantExecutorExtension())
    override fun isolationMode() = IsolationMode.InstancePerTest

    private var points = 0

    init {
        describe("PointsCalculator") {
            val calculator = PointsCalculator(3)

            context("when no answer selected") {
                it("should return points") {
                    assertEquals(3, calculator.calculatePoints())
                }
            }

            context("on correct answer") {
                calculator.onAnswerCorrect()
                points = calculator.calculatePoints()

                it("should return points") {
                    assertEquals(3, points)
                }

                context("on correct answer") {
                    calculator.onAnswerCorrect()
                    points = calculator.calculatePoints()

                    it("should return points") {
                        assertEquals(6, points)
                    }
                }

                context("on wrong answer") {
                    calculator.onAnswerWrong()
                    points = calculator.calculatePoints()

                    it("should return points") {
                        assertEquals(5, points)
                    }
                }
            }

            context("on wrong answer") {
                calculator.onAnswerWrong()
                points = calculator.calculatePoints()

                it("should return points correctly") {
                    assertEquals(2, points)
                }

                context("on correct answer") {
                    calculator.onAnswerCorrect()
                    points = calculator.calculatePoints()

                    it("should return points correctly") {
                        assertEquals(5, points)
                    }
                }

                context("when reset") {
                    calculator.reset()
                    it("should return maximum") {
                        assertEquals(3, calculator.calculatePoints())
                    }
                }

                context("when wrong answer selected again") {
                    calculator.onAnswerWrong()
                    points = calculator.calculatePoints()

                    it("should return points correctly") {
                        assertEquals(3, points)
                    }

                    context("on correct answer") {
                        calculator.onAnswerCorrect()
                        points = calculator.calculatePoints()

                        it("should return points correctly") {
                            assertEquals(6, points)
                        }
                    }

                    context("when reset") {
                        calculator.reset()
                        it("should return maximum") {
                            assertEquals(3, calculator.calculatePoints())
                        }
                    }
                }
            }

            context("on all wrong answers") {
                calculator.onAnswerWrong()
                calculator.onAnswerWrong()
                calculator.onAnswerWrong()

                it("should return points") {
                    assertEquals(0, calculator.calculatePoints())
                }
            }
        }
    }
}
