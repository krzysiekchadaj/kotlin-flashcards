package com.kchadaj.flashcards

import com.kchadaj.flashcards.data.Repository
import com.kchadaj.flashcards.entities.Flashcard
import com.kchadaj.flashcards.entities.Word
import com.kchadaj.flashcards.items.AnswerItem
import com.kchadaj.flashcards.utils.extensions.LiveDataInstantExecutorExtension
import com.kchadaj.flashcards.utils.label.Label
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import io.kotlintest.IsolationMode
import io.kotlintest.specs.DescribeSpec
import org.junit.Assert.assertEquals

class MainViewModelTest : DescribeSpec() {

    override fun listeners() = listOf(LiveDataInstantExecutorExtension())
    override fun isolationMode() = IsolationMode.InstancePerTest

    private val repository = mock<Repository> {
        on { getRandomFlashcard() } doReturn Flashcard(
            solution = Word("s_c", "s_p", "s_e"),
            answers = listOf(
                Word("a1_c", "a1_p", "a1_e"),
                Word("a2_c", "a2_p", "a2_e"),
                Word("a3_c", "a3_p", "a3_e"),
                Word("s_c", "s_p", "s_e")
            )
        )
    }

    init {
        describe("MainViewModel") {
            val viewModel = MainViewModel(repository)

            context("on init") {
                it("should fetch first flashcard") {
                    verify(repository).getRandomFlashcard()
                }

                it("should set primary text") {
                    assertEquals(
                        Label.of("s_e"),
                        viewModel.primaryText.value
                    )
                }
                it("should set secondary text") {
                    assertEquals(
                        Label.of("s_p"),
                        viewModel.secondaryText.value
                    )
                }
                it("should set secondary visibility") {
                    assertEquals(true, viewModel.secondaryVisibility.value)
                }
                it("should set answers") {
                    assertEquals(
                        listOf(
                            AnswerItem("a1_c"),
                            AnswerItem("a2_c"),
                            AnswerItem("a3_c"),
                            AnswerItem("s_c")
                        ),
                        viewModel.answerItems.value
                    )
                }
                it("should set points") {
                    assertEquals(
                        Label.of(R.string.points_label, 0),
                        viewModel.pointsLabel.value
                    )
                }
            }

            context("on correct answer click") {
                viewModel.onItemClick(AnswerItem("s_c"))

                it("should fetch new flashcard") {
                    verify(repository, times(2)).getRandomFlashcard()
                }

                it("should update points label") {
                    assertEquals(
                        Label.of(R.string.points_label, 3),
                        viewModel.pointsLabel.value
                    )
                }
            }

            context("on wrong answer click") {
                viewModel.onItemClick(AnswerItem("a1_c"))

                it("should update answers") {
                    assertEquals(
                        listOf(
                            AnswerItem("a1_c", false),
                            AnswerItem("a2_c"),
                            AnswerItem("a3_c"),
                            AnswerItem("s_c")
                        ),
                        viewModel.answerItems.value
                    )
                }

                it("should not update points") {
                    assertEquals(
                        Label.of(R.string.points_label, 0),
                        viewModel.pointsLabel.value
                    )
                }

                context("on reset") {
                    viewModel.reset()

                    it("should update answers") {
                        assertEquals(
                            listOf(
                                AnswerItem("a1_c"),
                                AnswerItem("a2_c"),
                                AnswerItem("a3_c"),
                                AnswerItem("s_c")
                            ),
                            viewModel.answerItems.value
                        )
                    }

                    it("should update points label") {
                        assertEquals(
                            Label.of(R.string.points_label, 0),
                            viewModel.pointsLabel.value
                        )
                    }
                }

                context("on correct answer click") {
                    viewModel.onItemClick(AnswerItem("s_c"))

                    it("should fetch new flashcard") {
                        verify(repository, times(2)).getRandomFlashcard()
                    }

                    it("should update points label") {
                        assertEquals(
                            Label.of(R.string.points_label, 2),
                            viewModel.pointsLabel.value
                        )
                    }
                }

                context("on wrong answer click") {
                    viewModel.onItemClick(AnswerItem("a2_c"))

                    it("should update answers") {
                        assertEquals(
                            listOf(
                                AnswerItem("a1_c", false),
                                AnswerItem("a2_c", false),
                                AnswerItem("a3_c"),
                                AnswerItem("s_c")
                            ),
                            viewModel.answerItems.value
                        )
                    }

                    it("should not update points") {
                        assertEquals(
                            Label.of(R.string.points_label, 0),
                            viewModel.pointsLabel.value
                        )
                    }

                    context("on reset") {
                        viewModel.reset()

                        it("should update answers") {
                            assertEquals(
                                listOf(
                                    AnswerItem("a1_c"),
                                    AnswerItem("a2_c"),
                                    AnswerItem("a3_c"),
                                    AnswerItem("s_c")
                                ),
                                viewModel.answerItems.value
                            )
                        }

                        it("should update points label") {
                            assertEquals(
                                Label.of(R.string.points_label, 0),
                                viewModel.pointsLabel.value
                            )
                        }
                    }

                    context("on correct answer click") {
                        viewModel.onItemClick(AnswerItem("s_c"))

                        it("should fetch new flashcard") {
                            verify(repository, times(2)).getRandomFlashcard()
                        }

                        it("should update points label") {
                            assertEquals(
                                Label.of(R.string.points_label, 1),
                                viewModel.pointsLabel.value
                            )
                        }
                    }

                    context("on wrong answer click") {
                        viewModel.onItemClick(AnswerItem("a3_c"))

                        it("should update answers") {
                            assertEquals(
                                listOf(
                                    AnswerItem("a1_c", false),
                                    AnswerItem("a2_c", false),
                                    AnswerItem("a3_c", false),
                                    AnswerItem("s_c")
                                ),
                                viewModel.answerItems.value
                            )
                        }

                        it("should not update points") {
                            assertEquals(
                                Label.of(R.string.points_label, 0),
                                viewModel.pointsLabel.value
                            )
                        }

                        context("on reset") {
                            viewModel.reset()

                            it("should update answers") {
                                assertEquals(
                                    listOf(
                                        AnswerItem("a1_c"),
                                        AnswerItem("a2_c"),
                                        AnswerItem("a3_c"),
                                        AnswerItem("s_c")
                                    ),
                                    viewModel.answerItems.value
                                )
                            }

                            it("should update points label") {
                                assertEquals(
                                    Label.of(R.string.points_label, 0),
                                    viewModel.pointsLabel.value
                                )
                            }
                        }

                        context("on correct answer click") {
                            viewModel.onItemClick(AnswerItem("s_c"))

                            it("should fetch new flashcard") {
                                verify(repository, times(2)).getRandomFlashcard()
                            }

                            it("should update points label") {
                                assertEquals(
                                    Label.of(R.string.points_label, 0),
                                    viewModel.pointsLabel.value
                                )
                            }
                        }
                    }
                }
            }

            context("on hide pinyin") {
                viewModel.showPinyin(false)

                it("pinyin should be gone") {
                    assertEquals(false, viewModel.secondaryVisibility.value)
                }
            }
        }
    }
}
